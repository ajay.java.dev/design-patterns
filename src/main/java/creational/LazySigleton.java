package creational;

public class LazySigleton {
	private LazySigleton singleton = null;

	private LazySigleton() {

	}

	public LazySigleton getSigleton() {

		if (singleton == null) {
			return new LazySigleton();
		}
		return singleton;
	}



}
