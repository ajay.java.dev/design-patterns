package creational;

public class LazySynchedSingleton {
	private volatile LazySynchedSingleton singleton = null;

	private LazySynchedSingleton() {}

	public LazySynchedSingleton getSigleton() {
		if (singleton == null) {
			synchronized (LazySynchedSingleton.class) {
				if (singleton == null) {
					singleton = new LazySynchedSingleton();
				}
			}

		}
		return singleton;
	}
}
