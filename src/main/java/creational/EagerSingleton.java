package creational;

public class EagerSingleton {

  private static final EagerSingleton eagerSingleton = new EagerSingleton();

  private EagerSingleton() {}

  public static EagerSingleton getSingletonIntance() {
    return eagerSingleton;
  }

  public static void main(String[] args) {

    // this possible here because its the same class
    EagerSingleton eagerSingleton = new EagerSingleton();
  }
}
