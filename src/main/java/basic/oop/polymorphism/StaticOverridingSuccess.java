package basic.oop.polymorphism;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

// https://www.geeksforgeeks.org/can-we-overload-or-override-static-methods-in-java/
public class StaticOverridingSuccess extends StaticSuper {

  /*
  Static methods can not be overridden(Method Overriding vs Method Hiding)

  Notice that the below static method has the exact same signature as the
  parent, but overriding fails and @Override is throwing an error. This is
  because the method here now doesn't demonstrate's any overriding behaviour
  but basically demonstrates METHOD HIDING.

  Static binding is done during compile-time while dynamic binding is done during run-time.

  Note: Remove the override notation and compilation error is resolved.
  */
  // @Override
  public static void getInstance(String name) {
    System.out.println("StaticOverridingSuccess.getInstance -> name : " + name);
  }

  /*
  This is a regular overriding.
   */
  @Override
  public void getNewInstance(String name) {
    System.out.println("StaticOverridingSuccess.getNewInstance");
  }

  // The overriding method must have same return type (or subtype). Here we
  // have changed the return type but is the return type is the subclass of
  // the parent return type.
  @Override
  public LinkedList<String> getElements() {
    return new LinkedList<>();
  }

  // NOTE : Checked exception cannot be thrown, but unchecked (error / runtime)
  // exception can be thrown even when super method doesn't throw an
  // exception. The reverse however is possible.
  // 1) Error example : OutOfMemoryError or StackOverflow error. These should
  // not be handled and application should throw them.
  // 2) Runtime Exception: ArrayIndexOutOfBound, ClassCastException etc
  @Override
  protected List<String> getNewElements() throws RuntimeException {
    return new ArrayList<>();
  }
}
