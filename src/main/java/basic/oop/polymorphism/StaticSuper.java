package basic.oop.polymorphism;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class StaticSuper {

  private static volatile StaticSuper instance = null;

  /*
  Private constructor is done mainly for singleton, so that default constructor
  is not available.Sub-classes cannot extend super class if default
  constructor is not available.
  */

  // private StaticSuper() {}

  public static void getInstance(String name) {
    System.out.println("StaticSuper.getInstance -> name : " + name);
  }

  public void getNewInstance(String name) {
    System.out.println("StaticSuper.getNewInstance -> name : " + name);
  }

  protected List<String> getElements() {
    System.out.println("StaticSuper.getElements -> empty array returned");
    return new ArrayList<>();
  }

  protected List<String> getNewElements()  {
    System.out.println("StaticSuper.getElements -> empty array returned");
    return new ArrayList<>();
  }

  public static final StaticSuper getInstance() {
    if (instance == null) {
      synchronized (StaticSuper.class) {
        if (instance == null) {
          instance = new StaticSuper();
        }
      }
    }
    return instance;
  }
}
