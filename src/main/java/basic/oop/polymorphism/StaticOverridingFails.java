package basic.oop.polymorphism;

// https://www.geeksforgeeks.org/overriding-in-java/

// THE COMPILATION ERRORS BELOW ARE DONE PURPOSEFULLY FOR LEARNING...

public class StaticOverridingFails extends StaticSuper {

  // this demonstrates static method in super class cannot be overriden
  // with a instance method in subclass.
  /*


  public void getInstance(String name) {
  	System.out.println("StaticSuper.getInstance -> name : " + name);
  }

  //this demonstrates instance method in super class cannot be overriden
  // with a static method in subclass.
  public static void getNewInstance (String name) {
  	System.out.println("StaticSuper.getNewInstance -> name : " + name);
  }

  //The access modifier for an overriding method can allow more,
  // but not weaker, access than the overridden method.
  @Override
  private List<String> getElements() {
  	return super.getElements();
  }

  //Final methods cannot be overriden
  public static StaticSuper getInstance() {}

  @Override
  protected List<String> getNewElements() throws Exception {
  	return new ArrayList<>();
  }


  */
}
