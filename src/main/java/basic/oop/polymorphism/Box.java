package basic.oop.polymorphism;

public class Box {

	protected enum BOX_TYPE{
		ROUND_BOX, RECTANGLE_BOX, SPHERICAL_BOX, CYLINDRICAL_BOX
	}

	protected BOX_TYPE getBoxType () {
		return BOX_TYPE.RECTANGLE_BOX; // default box.
	}
}
