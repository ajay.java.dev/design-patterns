package basic.oop.polymorphism;

/**
 * https://www.geeksforgeeks.org/object-oriented-programming-oops-concept-in-java/#Polymorphism
 *
 * Polymorphism refers to the ability of OOPs programming languages to differentiate between
 * entities with the same name efficiently.
 *
 * This is done in Java with the help of the signature and declaration of
 * these entities.
 *
 * There is 2 types of polymorphism: overloading and overriding.
 *
 * 1) Overloading : example is below. different methods to have the same name,
 * but different signatures
 * where the signature can differ by the number of input parameters or type of input parameters or both.
 *
 * 2) Overriding : Method overriding is one of the way by which java achieves
 * Run Time Polymorphism --> https://www.geeksforgeeks.org/dynamic-method-dispatch-runtime-polymorphism-java/
 *
 * NOTE: Dynamic method dispatch is the mechanism by which a call to an overridden method is
 * resolved at run time, rather than compile time.
 *
 * - Static binding is done during compile-time while dynamic binding is done
 * during run-time.
 *
 * -
 */
public class PolymorphicBox extends Box{


	//Overloading Section ...
	public int getDimensions(int length, int width){
		//do something

		return 0;
	}

	public int getDimensions(int length, int width, int height){
		//do something

		return 0;
	}

	public String getDimensions(int length, int width, int height,
	                             String name){
		//do something
		return null;
	}

	//Overriding Section...

	@Override
	protected BOX_TYPE getBoxType() {
		return BOX_TYPE.ROUND_BOX;
	}
}
