package basic.oop.inheritance;

public class SubClassDog extends SuperClassAnimal{

	@Override
	public void eat(int quantity, int grow) {
		grow = grow + ((grow+quantity)/2);
	}
}
