package basic.oop.inheritance;

public class SubClassBird extends SuperClassAnimal{

	@Override
	public void eat(int quantity, int grow) {
		grow = grow + ((grow+quantity)/12);
	}
}
