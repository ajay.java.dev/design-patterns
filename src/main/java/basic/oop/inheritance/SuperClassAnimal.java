package basic.oop.inheritance;

import basic.oop.abstraction.Creature;

public class SuperClassAnimal extends Creature {

	private String name;
	private double height;
	private int weight;

	public void eat(int quantity, int grow) {
		if (quantity > 0) {
			grow = grow + ((quantity+grow)/10);
		}
	}

	public boolean equals(Object object) {
		if (this == object) return true;
		if (!(object instanceof SuperClassAnimal)) return false;
		if (!super.equals(object)) return false;
		SuperClassAnimal that = (SuperClassAnimal) object;
		return java.lang.Double.compare(that.getHeight(), getHeight()) == 0 &&
				getWeight() == that.getWeight() &&
				java.util.Objects.equals(getName(), that.getName());
	}

	public int hashCode() {
		return java.util.Objects.hash(super.hashCode(), getName(), getHeight(), getWeight());
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}
