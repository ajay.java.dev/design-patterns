package basic.oop.abstraction;

public abstract class Creature {

	//there are not abstract fields. only abstract class and methods...
	protected String name;
	protected String height;
	protected String weight;

	protected abstract void setName(String name);
	protected abstract void setHeight(double height);
	protected abstract void setWeight(int weight);

	public static String getClassName (){
		return Creature.getClassName();
	}

	public Creature getCreatureInstance () {
		return new Creature() {
			@Override
			protected void setName(String name) {

			}

			@Override
			protected void setHeight(double height) {

			}

			@Override
			protected void setWeight(int weight) {

			}
		};
	}
}
